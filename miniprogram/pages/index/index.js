// miniprogram/pages/index/index.js
import Dialog from './../../dist/dialog/dialog';
const db = wx.cloud.database()
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    booking: [
      //   {
      //   "room": "C座302",
      //   "type": "会议室",
      //   "note": "请在使用完之后将房间打扫干净",
      //   "time": "2020年4月2日 2:00 pm"
      // }
    ],
    roomList: [
      // {
      //   "room": "C座302",
      //   "type": "会议室",
      //   "time": "8:00-10:00",
      //   "people": 5
      // },
      // {
      //   "room": "图书馆102",
      //   "type": "研讨室",
      //   "time": "8:00-10:00",
      //   "people": 6
      // }, {
      //   "room": "教学楼302",
      //   "type": "研讨室",
      //   "time": "8:00-10:00",
      //   "people": 8
      // },
      // {
      //   "room": "C座302",
      //   "type": "会议室",
      //   "time": "8:00-10:00",
      //   "people": 5
      // },
      // {
      //   "room": "图书馆102",
      //   "type": "研讨室",
      //   "time": "8:00-10:00",
      //   "people": 6
      // },
    ],
    list: [],
  },
  formatDate: function (date) {
    date = new Date(date);
    return `${date.getFullYear()}年${date.getMonth() + 1}月${date.getDate()}日 ${date.getHours()}:${(date.getMinutes()>9)?date.getMinutes():'0'+date.getMinutes()}`;
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.cloud.callFunction({
        name: 'login',
        data: {},
      })
      .then(res => {
        console.log(res.result)
        app.globalData.openid=res.result.openid
        console.log("app.globalData",app.globalData)
        db.collection('User').where({
          _openid: res.result.openid
        }).get().then(result => {
          if(result.data.length!=0){
            console.log("获取到个人预定信息", result.data[0].booking)
            that.data.list = result.data[0].booking
          }else{
            Dialog.alert({
              title: '未登记',
              message: '点击确定按钮跳转到个人信息登记页面'
            }).then(() => {
              wx.navigateTo({
                url: "/pages/login/login?id=" + app.globalData.openid
              })
            });
          }
          
        }).then(res => {
          db.collection('RoomList').doc('currentDepartment').get().then(res => {
            for (var i = 0; i < res.data.RoomList.length; i++) {
              res.data.RoomList[i].time = "8:00-10:00"
            }
            this.setData({
              roomList: res.data.RoomList
            })
            var booking = []
            for (i in this.data.list) {
              var obj = {}
              obj.room = res.data.RoomList[this.data.list[i].roomId].room
              obj.type = res.data.RoomList[this.data.list[i].roomId].type
              obj.note = res.data.RoomList[this.data.list[i].roomId].note
              obj.time = this.formatDate(this.data.list[i].time)
              console.log(obj)
              booking.push(obj)
            }
            this.setData({
              booking: booking
            })
          })
        })
      })
      
  },


  cancel: function (e) {
    console.log(e.currentTarget.dataset.index)
    Dialog.confirm({
      title: '取消预约',
      message: '是否确认取消预约，取消后其他人将可预约该时间段。'
    }).then(() => {
      wx.cloud.callFunction({
        name: 'cancelRoom',
        data: {
          index:e.currentTarget.dataset.index
        },
      }).then(res=>{
        if(res.result.success==true){
          console.log("删除成功")
          wx.reLaunch({
            url: '/pages/index/index'
          })
        }
      })
    }).catch(() => {
      // on cancel
    });
  },

  roomOpen: function (e) {
    console.log(e.currentTarget.dataset.index)
    // console.log(this.data.booking[0].hasOwnProperty("rom"))
    wx.navigateTo({
      url: "/pages/roomPage/roomPage?id=" + e.currentTarget.dataset.index
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  scroll(e) {
    console.log(e)
  }
})
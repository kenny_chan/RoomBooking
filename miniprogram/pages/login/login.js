// miniprogram/pages/login/login.js
const db = wx.cloud.database()
const app = getApp();
import Toast from './../../dist/toast/toast';
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  finish: function () {
    var that=this
    if(this.data.name&&this.data.number&&this.data.college&&this.data.class&&this.data.tel){
      wx.getSetting({
        success(res) {
          if (!res.authSetting['scope.userInfo']) {
            wx.authorize({
              scope: 'scope.userInfo',
              success() {
              }
            })
          }
        }
      })
      var booking=[]
      db.collection('User').add({
        data:{
          _id:app.globalData.openid,
          name:that.data.name,
          number:that.data.number,
          college:that.data.college,
          class:that.data.class,
          tel:that.data.tel,
          booking:booking
        }
      }).then(res=>{
        wx.reLaunch({
          url: '/pages/index/index'
        })
      })
    }else{
      Toast('请完整填写个人信息');
    }
    
  },
  onChange(e) {
    // event.detail 为当前输入的值
    // console.log(e);
    var target = e.target.dataset.name
    this.setData({
      [target]: e.detail
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
// miniprogram/pages/roomPage/roomPage.js
const db = wx.cloud.database()
const _ = db.command
import Dialog from './../../dist/dialog/dialog';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    date: '',
    room: "加载中...",
    people: -1,
    description: "加载中...",
    show: false,
    type: [1, 1, 1, 1, 1, 1],
    typeFixed: [1, 1, 1, 1, 1, 1],
    select: -1,
    id:0,
    roomInfo:null,
    loading:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id
    console.log(id)
    var myDate = new Date()
    this.setData({
      date: this.formatDate(myDate),
      id:id
    })
    db.collection('RoomList').doc('currentDepartment').get().then(res => {
      console.log(res.data.RoomList[id])
      this.setData({
        room: res.data.RoomList[id].type + res.data.RoomList[id].room,
        people: res.data.RoomList[id].people,
        description: res.data.RoomList[id].description,
        roomInfo:res.data.RoomList[id]
      })
      if (res.data.RoomList[id].book[this.formatDate(myDate)]) {
        this.setData({
          type: res.data.RoomList[id].book[this.formatDate(myDate)],
          typeFixed: res.data.RoomList[id].book[this.formatDate(myDate)]
        })
      } else {
        this.setData({
          type: [1, 1, 1, 1, 1, 1],
          typeFixed: [1, 1, 1, 1, 1, 1]
        })
      }
    })


  },
  select: function (e) {
    // console.log(e.currentTarget.dataset.id)
    if (this.data.typeFixed[e.currentTarget.dataset.id] != 3) {
      if (this.data.type[e.currentTarget.dataset.id] == 1) {
        for (var i = 0; i < 6; i++) {
          this.data.type[i] = this.data.typeFixed[i]
        }
        this.setData({
          type: this.data.type,
          ['type[' + e.currentTarget.dataset.id + ']']: 2
        })
        this.data.select = e.currentTarget.dataset.id
      } else {
        this.setData({
          ['type[' + e.currentTarget.dataset.id + ']']: 1,
          select:-1
        })
      }
    }

  },
  book(){
    if(this.data.select==-1){
      Dialog.alert({
        title: '未指定时间',
        message: '请选择你要预定的时间段'
      }).then(() => {
        // on close
      });
    }else{
      wx.navigateTo({
        url: "/pages/book/book?id=" + this.data.id+"&time="+this.data.select+"&date="+this.data.date+"&room="+this.data.room
      })
    }
  },
  onShow1() {
    this.setData({
      show: true
    });
  },
  onClose() {
    this.setData({
      show: false
    });
  },
  formatDate(date) {
    date = new Date(date);
    return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`;
  },
  onConfirm(event) {
    this.setData({
      show: false,
      date: this.formatDate(event.detail)
    });
    if (this.data.roomInfo.book[this.data.date]) {
      console.log("该日期已存在")
      this.setData({
        type:this.data.roomInfo.book[this.data.date],
        typeFixed:this.data.roomInfo.book[this.data.date]
      })
    } else {
      console.log("该日期不存在，新建空白数组")
      this.setData({
        type:[1, 1, 1, 1, 1, 1],
        typeFixed:[1, 1, 1, 1, 1, 1]
      })
      
    }
  }
})
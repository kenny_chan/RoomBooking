// miniprogram/pages/book/book.js
import Dialog from './../../dist/dialog/dialog';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: " ",
    time: " ",
    select:0,
    date:"",
    name: "",
    tel: "",
    person: "",
    note: "",
    id:0,
  },

  finish: function () {
    wx.cloud.callFunction({
      name: 'bookRoom',
      data: {
        id:this.data.id,
        date:this.data.date,
        time:this.data.select,
        tel:this.data.tel,
        person:this.data.person,
        note:this.data.note,
        name:this.data.name
      },
    }).then(res=>{
      console.log(res.result.success)
      if(res.result.success==true){
        Dialog.alert({
          title: '预约成功',
          message: '您已成功预约该房间，请准时使用。'
        }).then(() => {
          wx.reLaunch({
            url: '/pages/index/index'
          })
        });
      }else{
        Dialog.alert({
          title: '预约失败',
          message: res.result.error
        }).then(() => {
          wx.reLaunch({
            url: '/pages/index/index'
          })
        });
      }
    })
  },

  onChange(e) {
    // event.detail 为当前输入的值
    // console.log(e);
    var target = e.target.dataset.name
    this.setData({
      [target]: e.detail
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.id=options.id
    this.data.date=options.date
    this.data.select=options.time
    var time = ""
    switch (options.time) {
      case '0':
        time = "8:00-10:00"
        break
      case '1':
        time = "10:00-12:00"
        break
      case '2':
        time = "14:00-16:00"
        break
      case '3':
        time = "16:00-18:00"
        break
      case '4':
        time = "18:00-20:00"
        break
      case '5':
        time = "20:00-22:00"
        break
    }
    this.setData({
      title: options.room,
      time:options.date+" "+time
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
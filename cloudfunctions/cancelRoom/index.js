const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database({
  // 该参数从 wx-server-sdk 1.7.0 开始支持，默认为 true，指定 false 后可使得 doc.get 在找不到记录时不抛出异常
  throwOnNotFound: false,
})
const _ = db.command

// event:{
//   index:"用户列表中该预约的序号"
// }
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  console.log("【event】",event)
  const RoomList = await db.collection('RoomList').doc('currentDepartment').get()
  const UserBooking = await db.collection('User').doc(wxContext.OPENID).get()

  if (RoomList && UserBooking){
    var list = RoomList.data.RoomList
    var book = UserBooking.data.booking
    var roomId=book[event.index].roomId
    var time =book[event.index].select
    var date=book[event.index].date
    list[roomId].book[date][time]=1
    book.splice(event.index,1)
    await db.collection('RoomList').doc('currentDepartment').update({
      data:{
        RoomList:list
      }
    }).then(res=>{
      console.log("RoomList修改成功",res)
    })
    await db.collection('User').doc(wxContext.OPENID).update({
      data:{
        booking:book
      }
    }).then(res=>{
      console.log("User修改成功",res)
    })
  }else{
    console.log("查询不到相关表单")
  }
  


  return {
    success:true
  }
}
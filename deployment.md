# 部署说明
### 下载项目
下载本项目或`git clone https://gitee.com/kenny_chan/RoomBooking.git`
### 导入到开发者工具
在微信小程序开发工具中选择该项目根目录并导入
### 部署项目基本信息
修改appid
project.config.json里修改appid

```
{
    // ...
    "appid": "自己的appid"
    // ...
}
```

### 部署云开发
部署`/cloudfunctions`中的云函数

### Bug 反馈
如果有 Bug ，请通过issue、底部点评，或者下面的联系方式反馈，非常感谢你的反馈！
### 联系方式
有任何问题，可以通过下方的联系方式联系我。
- 邮箱：kenny.yb.chan@outlook.com
### 详细介绍
关于项目的详细介绍请查看[README.md](https://gitee.com/kenny_chan/RoomBooking/blob/master/README.md)
# RoomBooking

RoomBooking是一个基于腾讯云开发的校园房间预约管理工具小程序。
### 需求
在大学生活中，我们在进行学生活动或学术交流等活动的时候，常常会遇到会议室、研讨室、实验室、自习室等房间的借用。除了图书馆中的研讨室学校会有官方的预约登记平台，许多学院楼中的房间常常没有统一的管理平台，借用的时候需要联系不同的负责人进行预约及登记，而且也不能直观的查看当前的借用情况，只能联系各个部门来获得当前可以借用的房间。这个效率低下并且浪费大量的时间。

### 功能 

在该小程序中，创建者可以创建各种类型的房间，并且提供房间介绍以及该房间最多可容纳人数。预约者在首页可以看到当前可以预约的会议室、研讨室等各类房间，并且选择需要预约的时间段，填写申请房间的用途、与会人员、备注等信息即可成功预约该房间。

### 技术方案

#### 前端框架：

整体基于原生实现，部分页面使用了Vant Weapp的组件，以便于能够快速开发并且提供稳定的性能。

#### 后端：

基于腾讯云开发——云端一体化的后端云服务 ，采用 serverless 架构，免去了移动应用构建中繁琐的服务器搭建和运维。使用了三个云函数来完成核心的预约登记功能。使用了云开发的文档型的 NoSQL 数据库来存储房间的信息以及预约信息。

## 项目截图

![首页](https://images.gitee.com/uploads/images/2020/0406/164727_2765ffd7_5479927.png "1.png")
![详情页](https://images.gitee.com/uploads/images/2020/0406/170529_6add9060_5479927.png "2.png")
![登记页](https://images.gitee.com/uploads/images/2020/0406/170546_956c9331_5479927.png "3.png")


## 部署教程
详情请查看[deployment.md](https://gitee.com/kenny_chan/RoomBooking/blob/master/deployment.md)
#### 下载项目
下载本项目或`git clone https://gitee.com/kenny_chan/RoomBooking.git`
#### 导入到开发者工具
在微信小程序开发工具中选择该项目根目录并导入
#### 部署项目基本信息
修改appid
project.config.json里修改appid

```
{
    // ...
    "appid": "自己的appid"
    // ...
}
```

#### 部署云开发
部署`/cloudfunctions`中的云函数


## 参与贡献

详情请查看[contributing.md](https://gitee.com/kenny_chan/RoomBooking/blob/master/contributing.md)
- Fork 本仓库
- 新建 Feat_xxx 分支
- 提交代码
- 新建 Pull Request


## LICENSE

[GNU Affero General Public License v3.0](https://gitee.com/kenny_chan/RoomBooking/blob/master/LICENSE)

## 联系方式

有任何问题，可以通过下方的联系方式联系我。

- 邮箱：[kenny.yb.chan@outlook.com](mailto:kenny.yb.chan@outlook.com)